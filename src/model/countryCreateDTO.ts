export class CountryCreateDTO {
  name: string;

  constructor(name: string) {
    this.name = name;
  }
}
